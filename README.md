A web based file manager for your local file system.

Why? Sometimes you can develop web sites locally but can't install executable files. Sometimes your really want bulk renaming and other features your OS file manager may not provide but can't  install [Total Commander](https://www.ghisler.com/) or something like it. When that happens, I ask myself, "Can't I write something that useful?"

Let's find out... :-)