<?php

require __DIR__ . '/vendor/autoload.php';

use FileManager\Classes\FileManager;

function fixPath($path) {
    $path = str_replace('\\', '\\\\', $path);
    return $path;
}

$path = 'C:\Users\mcnairy.michael.c\Pictures\mshanet\Newsletters';
$fm   = new FileManager(fixPath($path));
$fm->underscore();
