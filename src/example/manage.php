<?php

require __DIR__ . '/vendor/autoload.php';

use FileManager\Classes\FileManager;

$data = $_POST;
// print_r($data);
// die(__FILE__.":".__LINE__);
$fm   = new FileManager($data['directoryPath']);
// $fm->toUnderscore();
// $fm->getTree();

if(isset($data['case'])) {
    $fm->case($data['case']);
}

if(isset($data['spaces']) && $data['spaces'] == 'underscore') {
    $fm->underscore();
}