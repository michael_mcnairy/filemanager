<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Web File Manager</title>
    <link rel="stylesheet" href="/css/font-awesome.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
  </head>
  <body>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
        <h1>File Manager</h1>
      <!-- <i class="fa fa-folder-open" aria-hidden="true"></i> -->
          <form>
            <div class="form-group">
              <label for="directoryPath">Enter Path</label>
              <input required type="text" class="form-control" id="directoryPath" name="directoryPath" aria-describedby="directoryPath" placeholder="Enter full directory path ex: c:\tmp">
            </div>
            <div class="form-group">
              <label for="case"><span class="glyphicon glyphicon-text-heightSelect"></span>Case</label><br/>
              <input type="radio" name="case" value="lower" />Lower
              <input type="radio" name="case" value="upper"/>Upper
            </div>
            <div class="form-group">
              <label for"spaces">Convert Spaces</label><br/>
              <input type="checkbox" name="spaces" value="underscore"/>Underscores
              <input type="checkbox" name="spaces" value="dash"/>Dash
              </div>
            <div class="form-group">
                <input type="submit" id="leButton">
            </div>
          </form>
          <div id="directoryTree"></div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" integrity="sha384-3ceskX3iaEnIogmQchP8opvBy3Mi7Ce34nWjpBIwVTHfGYWQS9jwHDVRnpKKHJg7" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
    <script>
      $(document).ready(function() {
          var request = null;

          $("#leButton").on('click', function(event){
              event.preventDefault();
              var data = {};
              data.directoryPath = $("#directoryPath").val();
              data.case = $('input[name=case]:checked').val();
              data.spaces = $('input[name=spaces]:checked').val();

              request = $.ajax({
                method: "POST",
                url: "/manage.php",
                data: data
              });

              request.done(function(data) {
                  console.log(data);
              });
          });
      });
    </script>
  </body>
</html>
