<?php

/**
 * start out with basic file handling
 * refactor to use flysystem
 */
namespace FileManager\Classes;

class FileChangeLogger extends Base {
    /**
     * We don't want this changing after the class is instantiated
     */
    const BATCH_ID = time();

    private $_file = null;
    private $logPath = null;

    public function __construct() {
        
    }

    /**
     * Undo rename of files of batch id
     * Check that all files in the batch exist
     * If not all files of batch exist prevent operation and inform user
     * @param  integer $batchId Batch id number of previous renaming operation
     * @return string json
     */
    public function undo($batchId) {
        //
    }

    /**
     * Create text file with before and after rename names for undo
     * @return string json
     */
    private function create() {
        $this->_batchId = self::BATCH_ID;
        $this->_file = fopen($this->_batchId, 'w');
    }

    /**
     * Delete batch file for given batch id
     * @param  int $batchId Batch id number
     * @return string json 
     */
    public function delete($batchId) {
        unlink($batchId);
    }

    /**
     * Append entry to batch file
     * @param [type] $to   [description]
     * @param [type] $from [description]
     */
    public function append($to, $from) {
        $log = [
            'to' => $to,
            'from' => $from,
        ];

        $json = json_encode($log);

        fwrite($this->_file, $json);

        return $this->json_response();
    }
}
