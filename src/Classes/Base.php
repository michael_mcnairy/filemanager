<?php

namespace FileManager\Classes;

/**
 * TODO:
 * Special character lists : Unix, Linux, OS X
 */
class Base {
    /**
     * die and dump
     * @param  mixed $debug Anything to output to screen
     * @return string        debug info
     */
    protected static function dd($debug) {
        if(!empty($debug))
        {
            print_r($debug);
        }

        $debug = debug_backtrace();

        die("<p style='color: red;font-size: large'>" . __FILE__ . ":" . $debug[0]['line']."</p>");
    }

    protected function json_response($message = null, $code = 200) {
        // clear the old headers
        header_remove();

        // set the actual code
        http_response_code($code);

        // set the header to make sure cache is forced
        header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");

        // treat this as json
        header('Content-Type: application/json');
        $status = array(
            200 => '200 OK',
            400 => '400 Bad Request',
            422 => 'Unprocessable Entity',
            500 => '500 Internal Server Error'
        );

        // ok, validation error, or failure
        header('Status: '.$status[$code]);

        // return the encoded json
        return json_encode(array(
            'status' => $code < 300, // success or not?
            'message' => $message
            ));
    }

    /**
     * Check if operating system is case sensitive
     * @return boolean 
     */
    protected function isCaseSensitive() {
        $os = PHP_OS;

        switch ($os) {
            case 'WINNT':
                return false;
                break;
            case 'OSX':
                return false;
            default:
                return true;
                break;
        }
    }

    protected function getReservedCharacters() {
        $windowsReservedCharacters = [
            '<',
            '>',
            ':',
            '"',
            '/',
            '\\',
            '|',
            '?',
            '*',
        ];
    }
}