<?php

namespace FileManager\Classes;

/**
 * TODO
 * prevent special characters in file renames
 * rename (first get special Base special characters functions working)
 * undo rename w/bulk
 * filetree gui
 * image conversion w/bulk
 * undo image conversion
 * document conversion w/bulk
 * undo document conversion
 * drap and drop onto function
 * user-defined default settings
 */
class FileManager extends Base {
    private $_caseSensitive = null;
    private $_files         = null;
    private $_path          = null;

    public function __construct($path) {
        $this->_caseSensitive = $this->isCaseSensitive();
        $this->_path          = $this->sanitizePath($path);
        $this->_files         = scandir($this->_path);

        /**
         * get rid of current and one level up directores (. and ..)
         */
        unset($this->_files[0]);
        unset($this->_files[1]);

        /**
         * set current directory, some functions only accept file names, not full path to file
         */
        chdir($this->_path);
    }

    /**
     * Convert spaces in file and directory names to underscores. Does not overwrite existing filename.
     * @return void
     */
    public function underscore() {
        foreach ($this->_files as $file => $name) {
            $originalName = $this->_path . "\\" . $name;
            $newName = str_replace(' ', '_', $originalName);

            if(!file_exists($newName)) {
                rename($originalName, $newName);
                echo "Successfully renamed <strong>{$originalName}</strong> to <strong>{$newName}</strong><br/>";
            }
        }
    }

    /**
     * Convert file name to lowercase.
     * @param  string $case Set the case to 'upper' or 'lower', defaults to lower.
     * @return void
     */
    public function case($case = "lower") {
        foreach ($this->_files as $file => $name) {
            $originalName = $name;

            switch ($case) {
                case 'upper':
                    $newName  = strtoupper($originalName);
                    break;
                default:
                    $newName = strtolower($originalName);
                    break;
            }

            if($this->_caseSensitive) {
                if(!file_exists($newName)) {
                    rename($originalName, $newName);
                    echo "Successfully renamed <strong>{$originalName}</strong> to <strong>{$newName}</strong><br/>";
                }
            } else {
                //copy file with .lowercase appended to name
                $temp = $name . ".{$case}case";
                copy($name, $temp);

                //rename original with .bak appended to name
                $temp = $originalName . ".bak";
                rename($originalName, $temp);

                //rename .lowercase to lowercase version of name
                rename($originalName.".{$case}case", $newName);

                //remove .bak version
                unlink($originalName.".bak");
            }
        }
    }

    /**
     * Undo rename of files of batch id
     * Check that all files in the batch exist
     * If not all files of batch exist prevent operation and inform user
     * @param  integer $batchId Batch id number of previous renaming operation
     * @return void
     */
    public function undo($batchId) {

    }

    /**
     * Create text file with before and after rename names for undo
     * @return [type] [description]
     */
    private function batch() {
        //create Batch class to deal with batches
    }

    public function getDirectories() {
        $directories = [];

        foreach($this->_files as $file => $name) {
            //get directories
            $dir = "{$this->_path}\\{$name}";

            if(is_dir($dir)) {
                array_push($directories, $name);
            }
        }

        return $directories;
    }

    public function getTree() {
        $directories = $this->getDirectories();
        $tree = [];

        foreach ($directories as $directory) {
            $files = scandir($directory);
            $tree[$directory] = $files;
        }

        echo $this->json_response($tree);
    }

    /**
     * Return unordered list of files
     */
    public function list() {
        $html = '<ul style="list-style: none;">';

        foreach($this->_files as $file => $name) {
            $html .= <<< HTML
                <li>{$name}</li>
HTML;
        }

        $html .= "</ul>";

        return $html;
    }

    /**
     * Check syntax of path for current operating system and adjust accordingly
     * @param  string $path User-entered path
     * @return string       Programatically correct path
     */
    private function sanitizePath($path) {
        if(PHP_OS == "WINNT") {
            $path = str_replace('\\', '\\\\', $path);
        }

        return $path;
    }
}
