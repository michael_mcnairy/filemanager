<?php

namespace FileManager\Classes;

class ChangeTextCase extends Base {
    private $_text          = '';
    private $_caseSensitive = null;

    public function __construct($text, $caseSensitive = true) {
        $this->_text = $text;
        $this->_caseSensitive = $caseSensitive;
    }

     /**
     * Convert file name to lowercase.
     * @param  string $case Set the case to 'upper' or 'lower', defaults to lower.
     * @return void
     */
    public static function change($case = "lower") {
        foreach ($this->_files as $file => $name) {
            $originalName = $name;

            if(!$this->_caseSensitive) {
                if(!file_exists($newName)) {
                    rename($originalName, $newName);
                    echo "Successfully renamed <strong>{$originalName}</strong> to <strong>{$newName}</strong><br/>";
                }
            } else {
                //copy file with .lowercase appended to name
                $temp = $name . ".{$case}case";
                copy($name, $temp);

                //rename original with .bak appended to name
                $temp = $originalName . ".bak";
                rename($originalName, $temp);

                //rename .lowercase to lowercase version of name
                rename($originalName.".{$case}case", $newName);

                //remove .bak version
                unlink($originalName.".bak");
            }
        }
    }
}
